# import entire random package
import random

# import what we need from other packages
from bs4 import BeautifulSoup
from urllib.request import urlopen


# set up the HTML parser and retrieve the text
# from the website

def get_website_text(website):
    url = website
    page = urlopen(url)
    html = page.read().decode("utf-8")
    # soup = BeautifulSoup(html, "html.parser")
    # text = soup.get_text()
    # return text
    return BeautifulSoup(html, "html.parser")


def generate_password(soup: BeautifulSoup):
    # implementation for html page using tables to store the values

    # pull the values from the table elements
    table_contents = []
    for child in soup.find('table').tr.next_siblings:
        for td in child:
            # noinspection PyBroadException
            try:
                table_contents.append(td.text)
            except:
                continue
    # finalize wordlist for words greater than 5
    wordlist = [char for char in table_contents if len(char) > 4]

    # find random word
    selected_word = ""
    for i in range(0, 3):
        selected_word += wordlist[random.randrange(0, len(wordlist))]

    # change instances of random letter chosen for uppercase
    random_letter = selected_word[random.randrange(0, len(selected_word))]
    selected_word = selected_word.replace(random_letter, random_letter.upper())

    # add random number to word
    selected_word += str(random.randint(1, 99))

    return selected_word


def main():
    url = "https://www.worldclasslearning.com/english/4000-most-common-english-words.html"
    result = get_website_text(url)
    password = generate_password(result)
    print(password)


if __name__ == '__main__':
    main()
